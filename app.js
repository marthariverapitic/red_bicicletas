require('newrelic');
require('dotenv').config();
var createError= require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session  = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);

const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var authAPIRouter  = require('./routes/api/auth');
const Usuario  = require('./models/usuario');
const Token  = require('./models/token');


let store;

if(process.env.NODE_ENV ==='development'){
  //objeto store
   store = new session.MemoryStore;//queda en la memoria del servidor 
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection:'sessions'
  });
  store.on('error', function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}
var app = express();

app.set('setcretKey','jwt_pdw_!!223344')

app.use(session({
//propiedad de la cookie
  cookie:{ maxAge:240 * 60 * 60 * 100 } ,//tiempo de expiracion de la cookie
  store:store,    //donde se guarda la sesion
  saveUninitialized:true,      //atributo
  resave:'true',
  secret: 'red_bicis' //generar identificador de la cookie guarda datos encriptados
}));

var mongoose = require('mongoose');
//archivo configuracion moongoDB
var mongoBD = process.env.MONGO_URI;//identifica la conexion string del ambiente local
//var mongoBD= 'mongodb+srv://admin:pGlA4K1ir01xTbSv@red-bicis-dwlyz.mongodb.net/test?retryWrites=true&w=majority';
//conexion
mongoose.connect(mongoBD, { useNewUrlParser:true});//compatibilidad
mongoose.Promise= global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console,'mongodb conection error:'));//hookear 
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


app.get('/login', function(req, res){
   res.render('session/login');
});

app.post('/login', function(req, res, next){
 //passport
  passport.authenticate('local', function(err, usuario, info){
    
    if(err) return next(info);    
    if(!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});


app.get('/forgotPassword', function(req, res){
  res.render('session/forgotPassword');
});

app.post('/forgotPassword', function(req, res){
  Usuario.findOne({ email: req.body.email}, function(err,usuario){
    if(!usuario)  return res.render('session/forgotPassword', {info: {message: 'No existe'}});

    usuario.resetPassword(function(err){
      if(err) return next(err);
      console.log('session/forgotPAsswordMEssage');
    });
    res.render('session/forgotPasswordMessage');
  })
});

app.get('/resetPassword/:token', function(req,res, next){
  Token.findOne({ token: req.params.token}, function(err,token){
    if(!token) return res.status(400).send({ type: 'not-verified', msg: 'no Existe token'});

    Usuario.findById(token._userId, function(err, usuario){
      if(!usuario) return res.status(400).send({msg:'No existe un usuario'});
      res.render('session/resetPassword', {errors:{}, usuario:usuario});
    });
  });
});


app.post('/resetPassword', function(req,res){
  if(req.body.password != req.body.confirm_password){
    res.render('session/resetPassword', { errors: {confirm_password:{message:'no coincide el password'}},
    usuario: new Usuario({email:req.body.email})});
    return;    
  }
  Usuario.findOne({email: req.body.email}, function(err, usuario){
    usuario.password= req.body.password;
    usuario.save(function(err){
      if(err){
        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario})
      }else{
        res.redirect('/login');
      }
    })
  });
});

app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
//amtes  entrar ala ruta, pedira que te loguees
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas', validarUsuario,bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);

app.use('/privacy_policy',function(req,res){
  res.sendFile('public/policy_privacy.html');
});

app.use('/google89eac554484f2545',function(req,res){
  res.sendFile('public/google89eac554484f2545.html');
});

app.get('/auth/google',
  passport.authenticate('google', { scope: [
  'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/plus.profile.emails.read' ]
  }
));
app.get('/auth/google/callback', passport.authenticate('google',{
    succesRedirect:'/',
    failureRedirect:'/error'
  })
);

app.use('/', indexRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req,res,next){
  if(req.user){
    next();
  }else {
    console.log('user sin loguearse');
    res.redirect('/login');
  }
};

function validarUsuario (req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err){
      res.json({status:"error", message: err.message, data:null});
    }else{
      req.body.userId = decoded.id;
      console.log('jwt verify:' + decoded);
      next();
    }
  });
}
module.exports = app;
