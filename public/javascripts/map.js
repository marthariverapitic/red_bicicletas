/*var map = L.map('main_map').setView([-36.6012424, -58.3861497], 13);//fuuncion que pasa el id al mapa a la L

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{//especifica los atributos del mapa
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);//agrega la capa al objeto tile




$.ajax({
    dataType: "json",
    url:"api/bicicletas",
    success: function(result){
        console.log("entro");
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})*/

var map = L.map('main_map').setView([-16.4051838, -71.5833646], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);



$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map)
        });
    }
});